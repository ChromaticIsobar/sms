import os
import sys
from itertools import filterfalse


python = "python" if len(sys.argv) < 2 else sys.argv[1]
os.system("rm -rf $(ls | grep -v 'sms-tools\|setup.py' | column) && cp -r sms-tools/software . && mv software sms && rm -rf sms/*_interface/")
for d in filterfalse(lambda x: x.split("/")[-1] == "utilFunctions_C", map(lambda x: x[0], os.walk("sms"))):
	os.system("echo \"import sys\nsys.path.append('{0}')\" > {0}/__init__.py".format(d))
os.system("cd sms/models/utilFunctions_C && {} compileModule.py build_ext --inplace".format(python))
os.system("mv sms/* . && rm -rf sms && cp sms-tools/README.md .")
